var searchData=
[
  ['sat_5fmax_0',['sat_max',['../classclosedloop_1_1_closed_loop.html#aa3332ec9433fb16b0d38adb01b2573e2',1,'closedloop::ClosedLoop']]],
  ['sat_5fmin_1',['sat_min',['../classclosedloop_1_1_closed_loop.html#a1b583e3f7f0b898fe4996be300f0166d',1,'closedloop::ClosedLoop']]],
  ['set_5fcalib_5fcoeff_2',['set_calib_coeff',['../class_i_m_u_1_1_i_m_u.html#a78a2c8956d53dcdb585ba0ddd0da4f77',1,'IMU::IMU']]],
  ['set_5fduty_3',['set_duty',['../class_d_r_v8847_1_1_motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847::Motor']]],
  ['set_5fkp_4',['set_Kp',['../classclosedloop_1_1_closed_loop.html#a617a88880b37c7434947936e1d3a37ce',1,'closedloop::ClosedLoop']]],
  ['set_5fopr_5fmode_5',['set_opr_mode',['../class_i_m_u_1_1_i_m_u.html#ab0349a88d720edda0e6522ebedf07a3d',1,'IMU::IMU']]],
  ['set_5fposition_6',['set_position',['../classencoder_1_1_encoder.html#a097746ac59abf28e6567f5604fe83c1f',1,'encoder::Encoder']]],
  ['share_7',['Share',['../classshares_1_1_share.html',1,'shares']]],
  ['shares_8',['shares',['../namespaceshares.html',1,'']]],
  ['speeds_9',['speeds',['../classtask__motor__4_1_1_task___motor.html#a99c23af7b18bef401b88d117d13e5627',1,'task_motor_4.Task_Motor.speeds()'],['../classtask__user__4_1_1_task___user.html#a56bc270f7b8fd7ce69d62cd1418b0558',1,'task_user_4.Task_User.speeds()']]]
];
