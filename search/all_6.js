var searchData=
[
  ['gains_0',['gains',['../classtask__motor__4_1_1_task___motor.html#acaec1a3a7bd277e71b8835d8c2406990',1,'task_motor_4.Task_Motor.gains()'],['../classtask__user__4_1_1_task___user.html#a73076087c426e6d80983754254665ea5',1,'task_user_4.Task_User.gains()']]],
  ['get_1',['get',['../classshares_1_1_queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares::Queue']]],
  ['get_5fangles_2',['get_angles',['../class_i_m_u_1_1_i_m_u.html#a869ce6e8ccc9815adab4879d43272128',1,'IMU::IMU']]],
  ['get_5fcalib_5fcoeff_3',['get_calib_coeff',['../class_i_m_u_1_1_i_m_u.html#a9544d0cc58985ead54a79109d9158f27',1,'IMU::IMU']]],
  ['get_5fcalib_5fstat_4',['get_calib_stat',['../class_i_m_u_1_1_i_m_u.html#a5500deed20bbec581b47451de998bf55',1,'IMU::IMU']]],
  ['get_5fdelta_5',['get_delta',['../classencoder_1_1_encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder::Encoder']]],
  ['get_5fkp_6',['get_Kp',['../classclosedloop_1_1_closed_loop.html#aabfd126eb373a40747f7fd312ed0056c',1,'closedloop::ClosedLoop']]],
  ['get_5fomegas_7',['get_omegas',['../class_i_m_u_1_1_i_m_u.html#a4c52c843c07676be0f2015940ef88d7a',1,'IMU::IMU']]],
  ['get_5fposition_8',['get_position',['../classencoder_1_1_encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder::Encoder']]]
];
